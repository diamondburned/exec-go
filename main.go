package main

import (
	"net/http"
	"os"
	"os/exec"
	"strings"
)

var addr = "127.0.0.1:8080"

func main() {
	if a := os.Getenv("ADDR"); a != "" {
		addr = a
	}

	http.HandleFunc("/", api)
	if err := http.ListenAndServe(addr, nil); err != nil {
		println(err.Error())
		os.Exit(1)
	}
}

func api(w http.ResponseWriter, r *http.Request) {
	var command string

	if r.Method == "GET" || r.Method == "POST" {
		command = r.FormValue("command")
		if command == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("?command= cannot be empty"))
		}
	}

	switch r.Method {
	case "GET":
		path, err := exec.LookPath(command)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(path))

	case "POST":
		var (
			parts   = strings.Fields(command)
			command = parts[0]
			args    = []string{}
		)

		if len(parts) > 1 {
			args = parts[1:]
		}

		cmd := exec.Command(command, args...)

		if err := cmd.Start(); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}

		w.WriteHeader(200)

	default:
		w.WriteHeader(404)
		w.Write([]byte("GET/POST only"))
	}
}
